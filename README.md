# Service Usage

* HZDR Gitlab / Codebase

## Plotting

* ... *Plotting is be performed in the ...*

## Data + Weighing

| Data | Weighing |
| ----- | ----- |
| number of users (total) | 25% |
| number of active users in last day | 25% |
| number of projects | 12,5% |
| number or commits (total) | 12,5% |
| storage size (total) | 12,5% |
| number of commits in last day | 12,5% |

## Schedule

* daily 
